package ar.fiuba.tdd.tp1.repl;

import ar.fiuba.tdd.tp1.model.Excel;

import java.util.HashMap;
import java.util.List;

public class Repl {

    private static final String COMMAND_CREATE = "createdoc";
    private static final String COMMAND_SET_CELL_VALUE = "set";
    private static final String COMMAND_GET_CELL_VALUE = "getcell";
    private static final String COMMAND_GET_SHEETS_FOR_DOC = "getsheets";
    private static final String COMMAND_GET_DOCS = "getDocs";
    private static final String COMMAND_CREATE_SHEET_FOR_DOC = "createsheet";
    private static final String COMMAND_UNDO = "undo";
    private static final String COMMAND_REDO = "redo";
    private static final String COMMAND_SET_CELL_FORMATTER = "setformatter";
    private static final String COMMAND_SET_CELL_TYPE = "settype";
    private static final String COMMAND_PERSIST_DOC = "persist";
    private static final String COMMAND_RELOAD_DOC = "reload";
    private static final String COMMAND_CURRENT_SHEET = "currentsheet";
    private static final String COMMAND_CREATE_RANGE = "range";

    Excel exel;
    HashMap<String, Command> commands;

    public Repl(Excel excel) {
        exel = excel;
        commands = new HashMap<String, Command>();
        initializeCommands();
    }

    private void initializeCommands() {
        intitializeCommandsSet();
        initializeCommandsCreate();
        initializeCommandsGet();
        commands.put(COMMAND_PERSIST_DOC, new Command() {
            @Override
            public void execute(Excel exel, String[] args) {
                ejecutePersistDoc(exel, args);
            }
        });
        commands.put(COMMAND_REDO, new Command() {
            @Override
            public void execute(Excel exel, String[] args) {
                exel.redo();
            }
        });
        commands.put(COMMAND_UNDO, new Command() {
            @Override
            public void execute(Excel exel, String[] args) {
                exel.undo();
            }
        });
        commands.put(COMMAND_RELOAD_DOC, new Command() {
            @Override
            public void execute(Excel exel, String[] args) {
                ejecuteReloadDoc(exel, args);
            }
        });

    }

    private void initializeCommandsGet() {
        commands.put(COMMAND_GET_CELL_VALUE, new Command() {
            @Override
            public void execute(Excel exel, String[] args) {
                System.out.println(":="
                        + exel.getCurrentSheet().get(args[2])
                                .getValueAsString());
            }
        });
        commands.put(COMMAND_CURRENT_SHEET, new Command() {
            @Override
            public void execute(Excel exel, String[] args) {
                ejecuteCurrentSheet(exel, args);
            }
        });
        commands.put(COMMAND_GET_DOCS, new Command() {
            @Override
            public void execute(Excel exel, String[] args) {
                ejecuteGetDocs(exel);
            }
        });
        commands.put(COMMAND_GET_SHEETS_FOR_DOC, new Command() {
            @Override
            public void execute(Excel exel, String[] args) {
                ejecuteGetSheetsForDoc(exel, args);
            }
        });

    }

    private void initializeCommandsCreate() {
        commands.put(COMMAND_CREATE, new Command() {
            @Override
            public void execute(Excel exel, String[] args) {
                exel.createNewWorkBookNamed(args[0]);
            }
        });
        commands.put(COMMAND_CREATE_SHEET_FOR_DOC, new Command() {
            @Override
            public void execute(Excel exel, String[] args) {
                ejecuteCreateSheetInDoc(exel, args);
            }
        });
        commands.put(COMMAND_CREATE_RANGE, new Command() {
            @Override
            public void execute(Excel excel, String[] args) {
                executeNewRange(excel,args);
            }
        });
    }

    private void intitializeCommandsSet() {
        commands.put(COMMAND_SET_CELL_VALUE, new Command() {
            @Override
            public void execute(Excel exel, String[] args) {
                ejecuteSetValue(exel, args);
            }
        });
        commands.put(COMMAND_SET_CELL_FORMATTER, new Command() {
            @Override
            public void execute(Excel exel, String[] args) {
                ejecuteSetCellFormatter(exel, args);
            }
        });
        commands.put(COMMAND_SET_CELL_TYPE, new Command() {
            @Override
            public void execute(Excel exel, String[] args) {
                ejecuteSetCellType(exel, args);
            }
        });
    }
    
    private void executeNewRange(Excel excel,String[] args) {
        if (args.length == 3) {
            excel.setNameRange(args[0], args[1], args[2]);
        } else {
            System.out.println("Error: invalid number of arguments");
        }
    }

    private static void ejecuteCurrentSheet(Excel exel, String[] args) {
        if (args.length == 2) {
            exel.setCurrentSheetOnDocument(args[0], args[1]);
            ;
        } else {
            System.out.println("Error: invalid number of arguments");
        }
    }

    private static void ejecuteReloadDoc(Excel exel, String[] args) {
        if (args.length == 1) {
            exel.reloadPersistedWorkBook(args[0]);
        } else {
            System.out.println("Error: invalid number of arguments");
        }
    }

    private static void ejecutePersistDoc(Excel exel, String[] args) {
        if (args.length == 2) {
            exel.persistWorkBook(args[0], args[1]);
        } else {
            System.out.println("Error: invalid number of arguments");
        }
    }

    private static void ejecuteSetCellFormatter(Excel exel, String[] args) {
        if (args.length == 5) {
            exel.setCellFormatter(args[0], args[1], args[2], args[3], args[4]);
            // doc,sheet,cell,formatter,format
            // Formatter: format,symbol,decimal
            // Format: ->for format: dmy,mdy
        } else {
            System.out.println("Error: cantidad de argumentos invalido");
        }
    }

    private static void ejecuteGetSheetsForDoc(Excel exel, String[] args) {
        if (args.length == 1) {
            List<String> sheetNames = exel.workSheetNamesFor(args[0]);
            System.out.println("Sheets for " + args[0] + ": ");
            for (String sheetName : sheetNames) {
                System.out.println("-> " + sheetName);
            }
        } else {
            System.out.println("Error: cantidad de argumentos invalido");
        }
    }

    private static void ejecuteSetCellType(Excel exel, String[] args) {
        if (args.length == 4) {
            exel.setCellType(args[0], args[1], args[2], args[3]);
        } else {
            System.out.println("Error: invalid number of arguments");
        }
    }

    private static void ejecuteCreateSheetInDoc(Excel exel, String[] args) {
        if (args.length == 2) {
            exel.createNewWorkSheetNamed(args[0], args[1]);
            // "Doc,new sheet"
        } else {
            System.out.println("Error: cantidad de argumentos invalido");
        }
    }

    private static void ejecuteGetDocs(Excel exel) {
        List<String> docNames = exel.workBooksNames();
        System.out.println("Documents: ");
        for (String docName : docNames) {
            System.out.println("-> " + docName);
        }
    }

    private static void ejecuteSetValue(Excel exel, String[] args) {
        // "tecnicas,default,A1,= 1 + 0 + 3.5 + -1"
        if (args.length == 4) {
            System.out.println(args[0] + " " + args[1] + " " + args[2] + " "
                    + args[3]);
            exel.setCellValue(args[0], args[1], args[2], args[3]);
        } else {
            System.out.println("Error: cantidad de argumentos invalido");
        }
    }

    public Command getCommand(String command) {
        return commands.get(command);
    }

}