package ar.fiuba.tdd.tp1.model.expressions.binary;

import ar.fiuba.tdd.tp1.model.value.Value;
import ar.fiuba.tdd.tp1.model.value.ValueHolder;
import ar.fiuba.tdd.tp1.model.value.ValueHolderImp;

public class ExpressionSub extends ExpressionBinary {

    public ExpressionSub(Value opSub1, Value opSub2) {
        super(opSub1, opSub2);
    }

    @Override
    public ValueHolder evaluate() {
        return new ValueHolderImp(op1.getValue().getNumerical() - (op2.getValue().getNumerical()));
    }
}