package ar.fiuba.tdd.tp1.model.expressions.nary;

import ar.fiuba.tdd.tp1.model.value.Value;
import ar.fiuba.tdd.tp1.model.value.ValueHolder;
import ar.fiuba.tdd.tp1.model.value.ValueHolderImp;

public class ExpressionRigth extends ExpressionNary {

	public ExpressionRigth(Value... ops) {
		super(ops);
	}

	@Override
	public ValueHolder evaluate() {

		Double op2d = ops[1].getValue().getNumerical();
		String str = ops[0].getValue().getAsString();

		return new ValueHolderImp(str.substring(str.length() - op2d.intValue(), str.length()));

	}

}
