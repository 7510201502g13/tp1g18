package ar.fiuba.tdd.tp1.model.expressions.nary;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import ar.fiuba.tdd.tp1.model.value.Value;
import ar.fiuba.tdd.tp1.model.value.ValueHolder;
import ar.fiuba.tdd.tp1.model.value.ValueHolderImp;

public class ExpressionPrintf extends ExpressionNary {

	public ExpressionPrintf(Value... ops) {
		super(ops);
	}

	@Override
	public ValueHolder evaluate() {

		Pattern pattern = Pattern.compile("\\$([0-9]+)");

		String text = ops[0].getValue().getAsString();

		Matcher matcher = pattern.matcher(text);

		for (int i = 1; i < ops.length; i++) {

			if (matcher.find()) {

				String toReplace = ops[Integer.valueOf(matcher.group(1))].getValue().getAsString();
				text = text.replaceAll("\\" + matcher.group(0), toReplace);
			}
		}

		return new ValueHolderImp(text);
	}

}
