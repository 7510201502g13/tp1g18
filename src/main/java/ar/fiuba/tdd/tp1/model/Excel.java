package ar.fiuba.tdd.tp1.model;

import ar.fiuba.tdd.tp1.deserialize.Deserializer;
import ar.fiuba.tdd.tp1.extern.driver.BadFormulaException;
import ar.fiuba.tdd.tp1.extern.driver.SpreadSheetTestDriver;
import ar.fiuba.tdd.tp1.serialize.Serializer;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class Excel implements SpreadSheetTestDriver {
    HashMap<String, Document> excel;
    Document currentDocument;
    Serializer serializer;
    Deserializer deserializer;
    String version;
    String projectName;

    public Excel() {
        excel = new HashMap<String, Document>();
        this.version = "1.1";
        this.projectName = "ExcelFiuba";
        this.serializer = new Serializer();
        this.deserializer = new Deserializer();
    }

    public Excel(String version, String projectName, List<Document> documents) {
        excel = new HashMap<String, Document>();
        this.version = version;
        this.projectName = projectName;
        for (Document document : documents) {
            excel.put(document.getDocumentName(), document);
        }
        this.serializer = new Serializer();
        this.deserializer = new Deserializer();
    }

    public String getVersion() {
        return this.version;
    }

    public String getProjectName() {
        return this.projectName;
    }

    @Override
    public List<String> workBooksNames() {
        ArrayList<String> documents = new ArrayList<>();
        documents.addAll(excel.keySet());
        return documents;
    }

    @Override
    public void createNewWorkBookNamed(String name) {
        currentDocument = new Document(name);
        excel.put(name, currentDocument);
    }

    @Override
    public void createNewWorkSheetNamed(String workbookName, String name) {
        excel.get(workbookName).addSpreedSheet(name);
    }

    @Override
    public List<String> workSheetNamesFor(String workBookName) {
        return excel.get(workBookName).getSpreedSheetsByName();
    }
    
    @Override
	public void setNameRange(String workBookName, String workSheetName, String expression){
    	excel.get(workBookName).setFunction(workSheetName, expression);
    }

    @Override
    public void setCellValue(String workBookName, String workSheetName, String cellName, String expression) {
    	expression=resolveNameRange(workBookName, workSheetName, expression);
        try {
            excel.get(workBookName).setExpression(workSheetName, cellName, expression);
        } catch (IllegalArgumentException e) {
            throw new BadFormulaException();
        }
    }

    @Override
    public String getCellValueAsString(String workBookName, String workSheetName, String cellName) {
        return excel.get(workBookName).getCellValue(workSheetName, cellName).getAsString();

    }

    @Override
    public double getCellValueAsDouble(String workBookName, String workSheetName, String cellName) {
        return excel.get(workBookName).getCellValue(workSheetName, cellName).getNumerical();
    }

    @Override
    public void undo() {
        currentDocument.undo();
    }

    @Override
    public void redo() {
        currentDocument.redo();
    }

    public Collection<Document> getDocuments() {
        return excel.values();
    }

    public Document getDocument(String documentName) {
        return excel.get(documentName);
    }

    public List<String> getCellsNamesFor(String document, String sheet) {
        return excel.get(document).getSpreedSheet(sheet).getCellsByName();
    }

    public SpreedSheet getCurrentSheet() {
        return this.currentDocument.getCurrentSpreedSheet();
    }

    public Document getCurrentDocument() {
        return this.currentDocument;
    }

    @Override
    public void setCellFormatter(String workBookName, String workSheetName, String cellId, String formatter, String format) {
        try {
            excel.get(workBookName).getSpreedSheet(workSheetName).get(cellId).getValue().setFormatter(formatter, format);
        } catch (ParseException e) {
            throw new BadFormulaException();
        }
    }

    @Override
    public void setCellType(String workBookName, String workSheetName, String cellId, String type) {
        try {
            excel.get(workBookName).getSpreedSheet(workSheetName).get(cellId).getValue().setType(type);
        } catch (ParseException e) {
            throw new BadFormulaException();
        }
    }

    @Override
    public void persistWorkBook(String workBookName, String fileName) {
        this.serializer.serializeInJson(this.getDocument(workBookName), fileName);
    }

    @Override
    public void reloadPersistedWorkBook(String fileName) {
        Document document = deserializer.deserializeJson(fileName);
        this.currentDocument = document;
        this.excel.put(document.getDocumentName(), document);
    }
    
    public void setCurrentSheetOnDocument(String document,String sheet) {
        excel.get(document).setCurrentSheet(sheet);
    }
    
	public String resolveNameRange(String workBookName, String workSheetName, String expression) {
		Iterator it = excel.get(workBookName).getSpreedSheet(workSheetName).nameRanges.entrySet().iterator();
		while (it.hasNext()) {
			Map.Entry pair = (Map.Entry) it.next();
			if (expression.contains(pair.getKey().toString())) {
				expression = expression.replaceAll(pair.getKey().toString(), pair.getValue().toString());
				// it.remove(); // avoids a ConcurrentModificationException\
			}
		}
		return expression;
	}

}
