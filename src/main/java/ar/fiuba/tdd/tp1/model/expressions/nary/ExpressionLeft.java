package ar.fiuba.tdd.tp1.model.expressions.nary;

import ar.fiuba.tdd.tp1.model.value.Value;
import ar.fiuba.tdd.tp1.model.value.ValueHolder;
import ar.fiuba.tdd.tp1.model.value.ValueHolderImp;

public class ExpressionLeft extends ExpressionNary {

    public ExpressionLeft(Value... ops) {
        super(ops);
    }

    @Override
    public ValueHolder evaluate() {
      
    	Double op2d=ops[1].getValue().getNumerical();
    	
        return new ValueHolderImp(ops[0].getValue().getAsString().substring(0,  op2d.intValue()));
    	
    }

}
