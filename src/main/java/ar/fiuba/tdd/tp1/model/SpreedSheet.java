package ar.fiuba.tdd.tp1.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

public class SpreedSheet {

    HashMap<String, Cell> cells;
    HashMap<String,String> nameRanges;
    String spreedSheetName;

    public SpreedSheet(String name) {
        cells = new HashMap<String, Cell>();
        nameRanges = new HashMap<String,String>();
        spreedSheetName = name;
    }

    public SpreedSheet(String spreedSheetName, List<Cell> cells) {
        this.cells = new HashMap<String, Cell>();
        this.spreedSheetName = spreedSheetName;
        for (Cell cell : cells) {
            this.cells.put(cell.getCellName(), cell);
        }

    }

    public String getSpreedSheetName() {
        return this.spreedSheetName;
    }

    public Collection<Cell> getCells() {
        return this.cells.values();
    }

    public List<String> getCellsByName() {
        ArrayList<String> cells = new ArrayList<String>();
        cells.addAll(this.cells.keySet());
        return cells;
    }

    public Cell get(String cellName) {
        if (cells.containsKey(cellName)) {
            return cells.get(cellName);
        } else {
            cells.put(cellName, new Cell(cellName));
            return cells.get(cellName);
        }
    }
}
