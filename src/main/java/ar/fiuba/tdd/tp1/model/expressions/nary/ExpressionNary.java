package ar.fiuba.tdd.tp1.model.expressions.nary;

import ar.fiuba.tdd.tp1.model.expressions.ExpressionBase;
import ar.fiuba.tdd.tp1.model.value.Value;

import java.util.LinkedList;


public abstract class ExpressionNary extends ExpressionBase {


    protected Value[] ops;

    public ExpressionNary(Value... ops) {
        listInvolved = new LinkedList<>();
        for (int i = 0; i < ops.length; i++) {
            listInvolved.addAll(ops[i].getCellsInvolve());
        }
        this.ops = ops;
    }


}
