package ar.fiuba.tdd.tp1.model.expressions;

import ar.fiuba.tdd.tp1.model.Cell;
import ar.fiuba.tdd.tp1.model.value.ValueHolder;

import java.util.List;

public abstract class ExpressionBase implements Expression {


    protected List<Cell> listInvolved;


    @Override
    public ValueHolder getValue() {
        return evaluate();
    }

    @Override
    public List<Cell> getCellsInvolve() {
        return listInvolved;
    }


}
