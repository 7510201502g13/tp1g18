package ar.fiuba.tdd.tp1.model.expressions;

import ar.fiuba.tdd.tp1.model.Cell;
import ar.fiuba.tdd.tp1.model.value.ValueHolder;
import ar.fiuba.tdd.tp1.model.value.ValueHolderImp;

import java.util.LinkedList;
import java.util.List;


public class ExpressionRange implements Expression {

    private ValueHolder value;
    private String alias;


    public ExpressionRange(String alias, String range) {
    	this.alias=alias;
        this.value=new ValueHolderImp(range);      
    }

    public String getAlias(){
    	return this.alias;
    }
    
    @Override
    public ValueHolder evaluate() {
        return value;
    }

    @Override
    public List<Cell> getCellsInvolve() {
        return new LinkedList<>();
    }

    @Override
    public ValueHolder getValue() {
        return value;
    }


}
