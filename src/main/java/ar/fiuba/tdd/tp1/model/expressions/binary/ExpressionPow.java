package ar.fiuba.tdd.tp1.model.expressions.binary;


import ar.fiuba.tdd.tp1.model.value.Value;
import ar.fiuba.tdd.tp1.model.value.ValueHolder;
import ar.fiuba.tdd.tp1.model.value.ValueHolderImp;

public class ExpressionPow extends ExpressionBinary {

    public ExpressionPow(Value opPow1, Value opPow2) {
        super(opPow1, opPow2);
    }

    @Override
    public ValueHolder evaluate() {
        return new ValueHolderImp(Math.pow(op1.getValue().getNumerical(),  op2.getValue().getNumerical()));
    }

}
