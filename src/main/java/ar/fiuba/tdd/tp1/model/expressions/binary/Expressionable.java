package ar.fiuba.tdd.tp1.model.expressions.binary;

public interface Expressionable {

    Expressionable subtract(Expressionable e1);

    Expressionable add(Expressionable e1);

    Expressionable mult(Expressionable e1);

    Expressionable concat(Expressionable e1);

    //T getValue();


}
