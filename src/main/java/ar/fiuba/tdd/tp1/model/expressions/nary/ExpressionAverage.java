package ar.fiuba.tdd.tp1.model.expressions.nary;

import ar.fiuba.tdd.tp1.model.value.Value;
import ar.fiuba.tdd.tp1.model.value.ValueHolder;
import ar.fiuba.tdd.tp1.model.value.ValueHolderImp;

public class ExpressionAverage extends ExpressionNary {

    public ExpressionAverage(Value... ops) {
        super(ops);
    }

    @Override
    public ValueHolder evaluate() {
        double sum = 0;
        for (int i = 0; i < ops.length; i++) {
            sum += ops[i].getValue().getNumerical();
        }

        return new ValueHolderImp(sum / ops.length);
    }

}
