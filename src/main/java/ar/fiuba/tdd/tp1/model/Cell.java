package ar.fiuba.tdd.tp1.model;

import ar.fiuba.tdd.tp1.extern.driver.BadReferenceException;
import ar.fiuba.tdd.tp1.model.expressions.Expression;
import ar.fiuba.tdd.tp1.model.expressions.ExpressionDefault;
import ar.fiuba.tdd.tp1.model.value.Value;
import ar.fiuba.tdd.tp1.model.value.ValueHolder;

import org.jgrapht.*;
import org.jgrapht.alg.CycleDetector;
import org.jgrapht.graph.DefaultDirectedGraph;
import org.jgrapht.graph.DefaultEdge;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;

public class Cell extends Observable implements Observer, Value {

    String cellName;
    String literalExpression;
    private Expression formula;
    private ValueHolder value;
    private List<Cell> subscriptions;
    private LinkedList<Expression> formulaUndo;
    private LinkedList<Expression> formulaRedo;

    public Cell() {
        this.literalExpression = "";
        formula = new ExpressionDefault(0);
        evaluateFormula();
        formulaRedo = new LinkedList<>();
        formulaUndo = new LinkedList<>();
        subscriptions = new LinkedList<>();
    }

    public Cell(String cellName) {
        this.literalExpression = "";
        this.cellName = cellName;
        formula = new ExpressionDefault(0);
        evaluateFormula();
        formulaRedo = new LinkedList<>();
        formulaUndo = new LinkedList<>();
        subscriptions = new LinkedList<>();
    }

    public void subscribeTo(Cell newSubscriber) {
        this.subscriptions.add(newSubscriber);
        newSubscriber.addObserver(this);
    }

    public String getCellName() {
        return this.cellName;
    }

    private void evaluateFormula() {
        this.value = this.formula.evaluate();
    }

    public void evaluate() {
        evaluateFormula();
        setChanged();
        notifyObservers();
    }

    @Override
    public void update(Observable obs, Object arg) {
        evaluate();
    }

    public ValueHolder getValue() {
        return value;
    }

    private void addToAdjacentMatrix(Map<Cell, List<Cell>> adjacentyMatrix) {
        
        if (adjacentyMatrix.containsKey(this)) {
            return;
        }
        List<Cell> adjacentCells = new ArrayList<Cell>();

        List<Cell> cellsInvolved = this.formula.getCellsInvolve();
        for (Cell cell : cellsInvolved) {
            System.out.println("Depends On:" + cell.cellName);
            adjacentCells.add(cell);
        }
        adjacentyMatrix.put(this, adjacentCells);

        for (Cell adjacentCell : adjacentCells) {
            adjacentCell.addToAdjacentMatrix(adjacentyMatrix);
        }
    }

    public void setExpression(Expression formula) throws BadReferenceException{

        removeSubscriptions();
        this.formula = formula;

        checkCycle(formula);


        List<Cell> cellsInvolved = this.formula.getCellsInvolve();
        for (Cell cell : cellsInvolved) {
            this.subscribeTo(cell);
        }

        update(this, null);
    }


    private void checkCycle(Expression formula) {
        Map<Cell,List<Cell>> adjacentyMatrix = new HashMap<Cell, List<Cell>>();
        //if(adjacentyMatrix.containsKey(this))return;

        addToAdjacentMatrix(adjacentyMatrix);

        DirectedGraph<Cell,DefaultEdge> graph = new DefaultDirectedGraph<Cell, DefaultEdge>(DefaultEdge.class);

        for (Cell cellKey : adjacentyMatrix.keySet()) {
            graph.addVertex(cellKey);
        }

        for (Cell cellKey : adjacentyMatrix.keySet()) {
            List<Cell> link = adjacentyMatrix.get(cellKey);
            for (Cell adjacentCell : link) {
                graph.addEdge( cellKey, adjacentCell );
            }
        }

        CycleDetector<Cell, DefaultEdge> detector = new CycleDetector<Cell, DefaultEdge>(graph);

        if (detector.detectCycles()) {
            //System.out.println("DETECTED MOTHER FUCKIN CYCLE");
            throw new BadReferenceException();
        }
    }

    private void removeSubscriptions() {
        for (Cell cell : subscriptions) {
            cell.deleteObserver(this);
        }
        subscriptions.clear();
    }

    @Override
    public List<Cell> getCellsInvolve() {
        //System.out.println("Cell:"+this.cellName);

        LinkedList<Cell> list = new LinkedList<>();
        list.add(this);
        return list;
    }

    public void undoExpression() throws NullPointerException {
        if (!formulaUndo.isEmpty()) {
            this.formulaRedo.add(this.formula);
            setExpression(formulaUndo.removeLast());
        }
    }

    public void redoExpression() throws NullPointerException {
        if (!formulaRedo.isEmpty()) {
            this.formulaUndo.add(this.formula);
            setExpression(formulaRedo.removeLast());
        }
    }


    public void addExpression(Expression formula) {
        this.formulaUndo.add(this.formula);
    }

    public void addExpressionAsString(String expression) {
        this.literalExpression = expression;
    }

    public String getExpressionAsString() {
        return this.literalExpression;
    }

    public String getValueAsString() {
        return this.value.getAsString();
    }

    @Override
    public String toString() {
        // TODO Auto-generated method stub
        return "Cell: " + this.cellName;//+" vaue: "+getValueAsString();
    }

}
