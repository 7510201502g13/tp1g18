package ar.fiuba.tdd.tp1.deserialize.json;

import com.google.gson.*;

import ar.fiuba.tdd.tp1.model.Cell;
import ar.fiuba.tdd.tp1.model.Document;
import ar.fiuba.tdd.tp1.model.SpreedSheet;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Iterator;

public class JsonDocumentDeserializer implements JsonDeserializer<Document> {

    @Override
    public Document deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        final JsonObject jsonDocument = json.getAsJsonObject();
        final String documentName = getPropertie("DocumentName", jsonDocument);
        final JsonArray jsonSheets = getArray("spreedSheets", jsonDocument);

        Document document = new Document(documentName);
        document.deleteDefault();
        setSheets(new ArrayList<SpreedSheet>(), jsonSheets.iterator(), document);
        return document;
    }

    private void setSheets(ArrayList<SpreedSheet> sheets, Iterator<JsonElement> itSheets, Document document) {
        while (itSheets.hasNext()) {

            JsonObject jsonSheet = getNext(itSheets);
            final String sheetName = getPropertie("SheetName", jsonSheet);
            final JsonArray jsonCells = getArray("cells", jsonSheet);
            document.addDeserializerSheet(sheetName);
            setCells(new ArrayList<Cell>(), jsonCells.iterator(), document, sheetName);


        }
    }

    private void setCells(ArrayList<Cell> cells, Iterator<JsonElement> itCells, Document document, String sheetName) {
        while (itCells.hasNext()) {
            final JsonObject jsonCell = getNext(itCells);
            final String cellName = getPropertie("Name", jsonCell);
            final String literalExpression = getPropertie("expression", jsonCell);
            document.deserializeExpression(sheetName, cellName, literalExpression);

        }
    }

    private JsonObject getNext(Iterator<JsonElement> itJson) {
        return itJson.next().getAsJsonObject();
    }

    private JsonArray getArray(String propertie, JsonObject json) {
        return json.get(propertie).getAsJsonArray();
    }

    private String getPropertie(String propertie, JsonObject json) {
        return json.get(propertie).getAsString();
    }
}
