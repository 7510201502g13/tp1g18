package ar.fiuba.tdd.tp1.serialize.json;

import com.google.gson.*;

import ar.fiuba.tdd.tp1.model.Cell;
import ar.fiuba.tdd.tp1.model.Document;
import ar.fiuba.tdd.tp1.model.SpreedSheet;

import java.lang.reflect.Type;

public class JsonDocumentSerializer implements JsonSerializer<Document> {

    JsonObject jsonDocument;

    public JsonDocumentSerializer() {
        jsonDocument = new JsonObject();
    }

    @Override
    public JsonElement serialize(Document document, Type arg1, JsonSerializationContext context) {
        final JsonArray jsonSpreedSheetArray = new JsonArray();

        for (final SpreedSheet spreedSheet : document.getSpreedSheets()) {
            setJsonObjectInArray(jsonSpreedSheetArray, serializeSpreedSheet(spreedSheet, new JsonObject()));
        }
        setProperty(jsonDocument, "DocumentName", document.getDocumentName());
        setArray(jsonDocument, "spreedSheets", jsonSpreedSheetArray);
        return jsonDocument;
    }

    private void setJsonObjectInArray(JsonArray jsonSpreedSheetArray, JsonElement jsonElement) {
        jsonSpreedSheetArray.add(jsonElement);

    }

    private JsonElement serializeSpreedSheet(SpreedSheet spreedSheet, JsonObject jsonSpreedSheet) {
        setProperty(jsonSpreedSheet, "SheetName", spreedSheet.getSpreedSheetName());

        final JsonArray jsonCellArray = new JsonArray();
        for (final Cell cell : spreedSheet.getCells()) {
            setJsonObjectInArray(jsonCellArray, serializeCell(cell, new JsonObject()));
        }
        setArray(jsonSpreedSheet, "cells", jsonCellArray);

        return jsonSpreedSheet;
    }

    private void setArray(JsonObject jsonObject, String property, JsonArray jsonArray) {
        jsonObject.add(property, jsonArray);

    }

    private JsonElement serializeCell(Cell cell, JsonObject jsonCell) {
        setProperty(jsonCell, "Name", cell.getCellName());
        setProperty(jsonCell, "expression", cell.getExpressionAsString());

        return jsonCell;
    }

    private void setProperty(JsonObject jsonObject, String property, String value) {
        jsonObject.addProperty(property, value);
    }


}
