package ar.fiuba.tdd.tp1.parser;

import ar.fiuba.tdd.tp1.model.expressions.Expression;

public interface Parser {
    Expression parse(String current, String expression) throws IllegalArgumentException;
}
