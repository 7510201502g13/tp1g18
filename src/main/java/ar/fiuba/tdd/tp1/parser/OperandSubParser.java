package ar.fiuba.tdd.tp1.parser;

import ar.fiuba.tdd.tp1.model.expressions.Expression;
import ar.fiuba.tdd.tp1.model.expressions.binary.ExpressionSub;

import java.util.Stack;

public class OperandSubParser extends OperandParser {

    OperandSubParser() {
        operandKey = "-";
    }

    @Override
    protected Expression buildFormula(Stack<Expression> operandStack) {

        return new ExpressionSub(operandStack.pop(), operandStack.pop());
    }
}
