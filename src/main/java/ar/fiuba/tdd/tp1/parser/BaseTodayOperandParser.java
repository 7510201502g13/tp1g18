package ar.fiuba.tdd.tp1.parser;

import java.util.Stack;

import ar.fiuba.tdd.tp1.model.expressions.Expression;

public class BaseTodayOperandParser implements ParserOperand {

	public boolean parse(String expression, Stack<Expression> operandStack, String current) {
		
		if (expression.equals("TODAY()")){
			ExpressionToday expressionToday= new ExpressionToday();
			operandStack.push(expressionToday);
			return true;
		}
		return false;
	}
	
}
