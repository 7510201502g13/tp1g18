package ar.fiuba.tdd.tp1.parser;

import ar.fiuba.tdd.tp1.model.Document;
import ar.fiuba.tdd.tp1.model.expressions.Expression;

public class FormulaCellParser extends CellParser {
	Document excelFiuba;

	public FormulaCellParser(Document excelFiuba) {

		this.excelFiuba = excelFiuba;
		setOperators();
	}

	protected void setOperators() {
		baseOperands.add(new BaseOperandCellParser(excelFiuba));
		
		
		baseOperands.add(new BaseOperandNumberParser());
		
		baseOperands.add(new BaseTodayOperandParser());
		baseOperands.add(new ParenthesesOperand(this));
		baseOperands.add(new MaxRangeOperand(this));
		baseOperands.add(new MaxValuesOperand(this));
		baseOperands.add(new MinValuesOperand(this));
		baseOperands.add(new MaxRangeOperand(this));
		baseOperands.add(new MinRangeOperand(this));
		baseOperands.add(new AverageRangeOperand(this));
		baseOperands.add(new LeftOperand(this));
		baseOperands.add(new RigthOperand(this));
		baseOperands.add(new PrintfOperand(this));

		operands.add(new OperandPowParser());
		operands.add(new OperandSubParser());
		operands.add(new OperandSumParser());
		
	}

	protected void setOnDestination(String destination, Expression formula, String current) {
		// BaseOperandCellParser cellParser = new
		// BaseOperandCellParser(excelFiuba);
		// Cell finalCell = cellParser.getCellFromExpression(destination);
		// finalCell.setFormula(formula);

		BaseOperandCellParser cellParser = new BaseOperandCellParser(excelFiuba);
		cellParser.getCellFromExpression(destination).setExpression(formula);

	}

}
