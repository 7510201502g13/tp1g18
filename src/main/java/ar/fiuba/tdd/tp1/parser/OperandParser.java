package ar.fiuba.tdd.tp1.parser;

import ar.fiuba.tdd.tp1.model.expressions.Expression;

import java.util.Stack;

public abstract class OperandParser implements ParserOperand {

    protected String operandKey;

    public boolean parse(String expression, Stack<Expression> operandStack, String current) {
        if (expression.compareTo(operandKey) == 0 && operandStack.size() > 1) {
            Expression newOperand = buildFormula(operandStack);
            operandStack.push(newOperand);
            return true;
        }

        return false;
    }

    protected abstract Expression buildFormula(Stack<Expression> operandStack);
}
