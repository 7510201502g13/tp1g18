package ar.fiuba.tdd.tp1.parser;

import java.util.Stack;

import ar.fiuba.tdd.tp1.model.expressions.Expression;
import ar.fiuba.tdd.tp1.model.expressions.nary.ExpressionRigth;
import ar.fiuba.tdd.tp1.model.value.Value;

public class RigthOperand extends ValuesOperand {
	public RigthOperand(Parser subParser) {
		super(subParser);
		preExp = "RIGTH";
	}

	@Override
	protected void buildExpression(Stack<Expression> operandStack, Value[] valueList) {
		operandStack.push(new ExpressionRigth(valueList));

	}
}
