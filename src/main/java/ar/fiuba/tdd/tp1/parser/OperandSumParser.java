package ar.fiuba.tdd.tp1.parser;

import ar.fiuba.tdd.tp1.model.expressions.Expression;
import ar.fiuba.tdd.tp1.model.expressions.binary.ExpressionSum;

import java.util.Stack;

public class OperandSumParser extends OperandParser {

    OperandSumParser() {
        operandKey = "+";
    }

    protected Expression buildFormula(Stack<Expression> operandStack) {
        return new ExpressionSum(operandStack.pop(), operandStack.pop());
    }
}
