package ar.fiuba.tdd.tp1.parser;

import ar.fiuba.tdd.tp1.model.expressions.Expression;
import ar.fiuba.tdd.tp1.model.expressions.nary.ExpressionPrintf;
import ar.fiuba.tdd.tp1.model.value.Value;

import java.util.Stack;

public class PrintfOperand extends ValuesOperand {
    PrintfOperand(Parser subParser) {
        super(subParser);
        preExp = "PRINTF";
    }

    @Override
    protected void buildExpression(Stack<Expression> operandStack,
            Value[] valueList) {
        operandStack.push(new ExpressionPrintf(valueList));
    }
}
