package ar.fiuba.tdd.tp1.parser;

import java.util.Stack;

import ar.fiuba.tdd.tp1.model.expressions.Expression;
import ar.fiuba.tdd.tp1.model.expressions.binary.ExpressionPow;

public class OperandPowParser extends OperandParser {

    OperandPowParser() {
        operandKey = "^";
    }

    protected Expression buildFormula(Stack<Expression> operandStack) {
        return new ExpressionPow(operandStack.pop(), operandStack.pop());
    }
}
