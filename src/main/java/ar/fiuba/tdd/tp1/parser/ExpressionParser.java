package ar.fiuba.tdd.tp1.parser;

import ar.fiuba.tdd.tp1.model.Document;
import ar.fiuba.tdd.tp1.model.expressions.Expression;
import ar.fiuba.tdd.tp1.model.expressions.ExpressionRange;
import ar.fiuba.tdd.tp1.parser.resolver.InvalidTypeResolver;
import ar.fiuba.tdd.tp1.parser.resolver.NumberResolver;
import ar.fiuba.tdd.tp1.parser.resolver.StringResolver;
import ar.fiuba.tdd.tp1.parser.resolver.TypeResolver;

/**
 * Created by Demian on 12/10/2015.
 */
public class ExpressionParser {

    static final String formulaKey = "=";
    CellParser formulaParser;
    CellParser stringParser;
    TypeResolver resolver;

    public ExpressionParser(Document excelFiuba) {
        formulaParser = new FormulaCellParser(excelFiuba);
        resolver = new NumberResolver(new StringResolver(new InvalidTypeResolver(null)));
        stringParser = new StringCellParser(excelFiuba);
    }

    public Expression parse(String actualSheet, String expression) {


        if (expression.contains(formulaKey)) {
            return parseAsFormula(actualSheet, expression);
        }

        //SEBAS SI LLEGA ACA ES PORQUE NO ES UNA FORMULA, NO TIENE EL =, HABRIA QUE VER SI ES UN NUMERO O STRING
        //        System.out.println("llego");

        return resolver.resolve(expression);
        //throw new IllegalArgumentException();

    }

    private Expression parseAsFormula(String actualSheet, String expression) {
        String[] splitedFormula = expression.split(formulaKey);

        try {
            return formulaParser.parse(actualSheet, formulaKey + splitedFormula[1]);
        } catch (IllegalArgumentException e) {
            return stringParser.parse(actualSheet, formulaKey + splitedFormula[1]);
        }
    }
    
    public ExpressionRange parseAsFunction(String actualSheet, String expression) {
        String[] splitedFormula = expression.split(formulaKey);
        try {
        	return new ExpressionRange(splitedFormula[0], splitedFormula[1]);
        } catch (IllegalArgumentException e) {
        	throw new IllegalArgumentException();
        }     		
        }
}
