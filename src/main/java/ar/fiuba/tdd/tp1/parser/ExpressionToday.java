package ar.fiuba.tdd.tp1.parser;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import ar.fiuba.tdd.tp1.model.Cell;
import ar.fiuba.tdd.tp1.model.expressions.Expression;
import ar.fiuba.tdd.tp1.model.value.ValueHolder;
import ar.fiuba.tdd.tp1.model.value.ValueHolderImp;

public class ExpressionToday implements Expression {

    private ValueHolder vh;

    @Override
    public ValueHolder getValue() {
        return vh;
    }

    @Override
    public List<Cell> getCellsInvolve() {
        return new ArrayList<Cell>();

    }

    @Override
    public ValueHolder evaluate() {

        vh = new ValueHolderImp(LocalDate.now().toString());
        return vh;
    }

}
