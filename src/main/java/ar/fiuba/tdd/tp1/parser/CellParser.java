package ar.fiuba.tdd.tp1.parser;


import ar.fiuba.tdd.tp1.model.expressions.Expression;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Stack;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public abstract class CellParser implements Parser {

    protected String splitKey = " ";

    protected ArrayList<ParserOperand> operands;
    protected ArrayList<ParserOperand> baseOperands;

    public CellParser() {
        operands = new ArrayList<ParserOperand>();
        baseOperands = new ArrayList<ParserOperand>();
    }

    protected abstract void setOperators();

    private Expression parse(List<String> operands, String actualSheet) throws IllegalArgumentException {

        Stack<Expression> operandStack = new Stack<Expression>();
        boolean[] parsedSuccesOperator = new boolean[operands.size()];

        parseBaseOperands(operands, parsedSuccesOperator, operandStack, actualSheet);

        parseOperands(operands, parsedSuccesOperator, operandStack, actualSheet);

        if (operandStack.size() == 0) {
            throw new IllegalArgumentException();
        }

        return operandStack.pop();
    }

    public Expression parse(String actualSheet, String expression) throws IllegalArgumentException {

        validateExpression(expression);

        List<String> operands = cleanExpression(expression);
        return parse(operands, actualSheet);
    }


    private List<String> cleanExpression(String expression) {
        List<String> operands = new ArrayList<String>();

        String[] splitedExpression = expression.split(splitKey);

        int subParentheses = 0;
        String expressionBuffer = "";

        for (int i = 0; i < splitedExpression.length; i++) {
            String currentExpression = splitedExpression[i];
            if (splitedExpression[i].length() > 0 && currentExpression.compareTo("=") != 0) {

                subParentheses += getNumberOfMatches(currentExpression, "\\(");
                subParentheses -= getNumberOfMatches(currentExpression, "\\)");

                if (subParentheses < 0) {
                    throw new IllegalArgumentException();
                }

                if (subParentheses > 0) {
                    expressionBuffer += currentExpression + " ";
                } else {
                    expressionBuffer += currentExpression;
                }

                if (subParentheses == 0) {
                    operands.add(expressionBuffer);
                    expressionBuffer = "";
                }
            }
        }

        return operands;
    }


    private void parseBaseOperands(List<String> operands, boolean[] parsedSuccesOperator, Stack<Expression> opStack, String actualSheet) {
        for (int i = operands.size() - 1; i >= 0; i--) {
            parsedSuccesOperator[i] = parseOperand(operands.get(i), opStack, baseOperands, actualSheet);
        }
    }

    private boolean parseOperand(String operand, Stack<Expression> operandStack, List<ParserOperand> operandsList, String current) {
        boolean succesOperatorParse = false;

        Iterator<ParserOperand> baseOperandsIterator = operandsList.iterator();

        while (!succesOperatorParse && baseOperandsIterator.hasNext()) {
            succesOperatorParse = baseOperandsIterator.next().parse(operand, operandStack, current);
        }

        return succesOperatorParse;
    }

    private void parseOperands(List<String> operands, boolean[] parsedSuccesOperator, Stack<Expression> operandStack, String current) {
        for (int i = 0; i < operands.size(); i++) {
            if (!parsedSuccesOperator[i] && !parseOperand(operands.get(i), operandStack, this.operands, current)) {
                throw new IllegalArgumentException();
            }
        }
    }

    private void validateExpression(String expression) throws IllegalArgumentException {
        if (expression == null) {
            throw new IllegalArgumentException();
        }

        if (expression.charAt(0) != '=') {
            throw new IllegalArgumentException();
        }
    }

    private int getNumberOfMatches(String expression, String subExpression) {
        Pattern pattern = Pattern.compile(subExpression);
        Matcher match = pattern.matcher(expression);

        int count = 0;
        //you will see that only 2 are the matchine string
        while (match.find()) {
            count++;
        }
        count++;
        return count;
    }

}
