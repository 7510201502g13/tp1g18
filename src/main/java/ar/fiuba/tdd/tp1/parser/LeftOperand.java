package ar.fiuba.tdd.tp1.parser;

import java.util.Stack;

import ar.fiuba.tdd.tp1.model.expressions.Expression;
import ar.fiuba.tdd.tp1.model.expressions.nary.ExpressionLeft;
import ar.fiuba.tdd.tp1.model.value.Value;

public class LeftOperand extends ValuesOperand {
	public LeftOperand(Parser subParser) {
		super(subParser);
		preExp = "LEFT";
	}

	@Override
	protected void buildExpression(Stack<Expression> operandStack, Value[] valueList) {
		operandStack.push(new ExpressionLeft(valueList));

	}
}
