package ar.fiuba.tdd.tp1.test.action;


import ar.fiuba.tdd.tp1.model.Excel;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;


public class UndoRedoTest {

    private static final float DELTA = 0.01f;
    private Excel excel;

    @Before
    public void setUp() {
        excel = new Excel();
        excel.createNewWorkBookNamed("Prueba");
        excel.createNewWorkSheetNamed("Prueba", "H1");
        excel.createNewWorkSheetNamed("Prueba", "H2");
        excel.createNewWorkSheetNamed("Prueba", "H3");
        excel.createNewWorkSheetNamed("Prueba", "H4");
        excel.setCellValue("Prueba", "H1", "A1", "= 6");
        excel.setCellValue("Prueba", "H1", "A1", "= 4");
    }

    @Test
    public void redoAloneDontDoNothing() {
        excel.redo();
    }


    @Test
    public void undoTest() {
        excel.undo();
        assertEquals(6f, excel.getCellValueAsDouble("Prueba", "H1", "A1"), DELTA);
        excel.undo();
        assertEquals(0f, excel.getCellValueAsDouble("Prueba", "H1", "A1"), DELTA);

    }

    @Test
    public void undoAndRedoTest() {
        excel.undo();
        assertEquals(6f, excel.getCellValueAsDouble("Prueba", "H1", "A1"), DELTA);
        excel.redo();
        assertEquals(4f, excel.getCellValueAsDouble("Prueba", "H1", "A1"), DELTA);
    }

    @Test
    public void multipleUndoTest() {
        excel.setCellValue("Prueba", "H2", "A3", "= !H1.A1 + 3");
        excel.setCellValue("Prueba", "H4", "A2", "= !H2.A3");

        excel.undo();
        assertEquals(7f, excel.getCellValueAsDouble("Prueba", "H2", "A3"), DELTA);
        assertEquals(0f, excel.getCellValueAsDouble("Prueba", "H4", "A2"), DELTA);
        assertEquals(4f, excel.getCellValueAsDouble("Prueba", "H1", "A1"), DELTA);
        excel.undo();
        assertEquals(0f, excel.getCellValueAsDouble("Prueba", "H2", "A3"), DELTA);
        assertEquals(4f, excel.getCellValueAsDouble("Prueba", "H1", "A1"), DELTA);
        excel.undo();
        assertEquals(6f, excel.getCellValueAsDouble("Prueba", "H1", "A1"), DELTA);
        excel.undo();
        assertEquals(0f, excel.getCellValueAsDouble("Prueba", "H1", "A1"), DELTA);
    }

    @Test
    public void multipleRedoTest() {
        excel.setCellValue("Prueba", "H2", "A3", "= !H1.A1 + 3");
        excel.setCellValue("Prueba", "H4", "A2", "= !H2.A3");

        excel.undo();
        excel.undo();
        excel.undo();
        excel.undo();
        assertEquals(0f, excel.getCellValueAsDouble("Prueba", "H1", "A1"), DELTA);
        excel.redo();
        assertEquals(6f, excel.getCellValueAsDouble("Prueba", "H1", "A1"), DELTA);
        excel.redo();
        assertEquals(4f, excel.getCellValueAsDouble("Prueba", "H1", "A1"), DELTA);
        excel.redo();
        assertEquals(7f, excel.getCellValueAsDouble("Prueba", "H2", "A3"), DELTA);
        excel.redo();
        assertEquals(7f, excel.getCellValueAsDouble("Prueba", "H4", "A2"), DELTA);
    }

    @Test
    public void multipleRedoUndoTest() {
        excel.setCellValue("Prueba", "H2", "A3", "= !H1.A1 + 3");
        excel.setCellValue("Prueba", "H4", "A2", "= !H2.A3");

        excel.undo();
        assertEquals(7f, excel.getCellValueAsDouble("Prueba", "H2", "A3"), DELTA);
        excel.redo();
        assertEquals(7f, excel.getCellValueAsDouble("Prueba", "H4", "A2"), DELTA);
        excel.undo();
        assertEquals(0f, excel.getCellValueAsDouble("Prueba", "H4", "A2"), DELTA);
        assertEquals(7f, excel.getCellValueAsDouble("Prueba", "H2", "A3"), DELTA);
        excel.undo();
        assertEquals(0f, excel.getCellValueAsDouble("Prueba", "H2", "A3"), DELTA);
        excel.redo();
        assertEquals(7f, excel.getCellValueAsDouble("Prueba", "H2", "A3"), DELTA);
        excel.redo();
        assertEquals(7f, excel.getCellValueAsDouble("Prueba", "H4", "A2"), DELTA);
    }
}
